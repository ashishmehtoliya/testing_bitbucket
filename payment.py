import pandas as pd
import csv

df1 = pd.read_csv('PaymentSheet.csv')
df2 = pd.read_csv('PaymentSheet2.csv')

df = pd.concat([df1 , df2])

final = pd.DataFrame(columns = ['OrderNum' , 'Profit/loss(%)' , 'Transfered Amount' , 'Total Marketplace Charges'])


for index , row in df.iterrows():

    OrderNum= row['OrderNum'];
    PLoss= (row['Sale Amount'] - row['Cost Price'])/row['Cost Price']*100
    TAmt = row['Transferred Amount']
    TMCharges = row['Transferred Amount'] + row['Payment Gateway'] + row['PickPack Fee']

    final = final.append({'OrderNum': OrderNum , 'Profit/loss(%)': PLoss, 'Transfered Amount': TAmt , 'Total Marketplace Charges': TMCharges },
     ignore_index=True)

final.to_csv('final2.csv'  , index_label='OrderNum')  #final2.csv in the same directory

